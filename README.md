# particle-management

This library provides an easy to use class with methods for connecting a Particle board to the network, publishing data and managing the connection and loops.

The code is written to compile in as little space as possible using multiple `#ifdef` checks based on the settings provided.

## Management class

The main component of the library is the `Management` class.

### Constructor

`Management()` The constructor for the class takes no argument and initializes the built-in variables.

### Variables

* `MQTT* MQTTClient``
The MQTT client (if enabled).

* `String project`
The project used for MQTT connection and topic root. Needs to be changed before calling `connect()`, otherwise defaults to "dbdlab".
* `String serial`
The serial number of the device taken from the system.
* `String name`
The name of the device, taken from Particle cloud, set to the serial number or set manually before calling `connect()`.
* `String topicRoot`
The topic root used to publish messages. Set up during connect as `[project]/[name]/`
* `String settings`
The settings used by the class in a semicolon-separated list.
* `int startTime`
The time the device booted up in Unix time (seconds since epoch).
* `int loop`
The number of loops that have run, updated by `loopStart()`
* `int loopEnd`
The time when the last loop ended, updated by `loopClose()`

### Methods

#### Connection

`int connect(int systemMode = 1)` Performs all the necessary steps to connect the device to the network and cloud based on the settings provided:

* Connects the device to the network. WiFi or cellular connection is selected depending on the device platform (argon, boron, etc...). If no `systemMode` argument is given or if a value different from 2 and 3 is passed, then connection is skipped as it cannot determine the mode of the device and whether connection calls are allowed.
* Connect to Particle cloud
* The device's name is taken from Particle cloud (if enabled and not already set).
* Connects to MQTT (if enabled).
* Sets the topic root for publishing as `[project]/[name]`.
* Sets the Bluetooth scan timeout (if enabled).
* Initializes the `startTime` variable with the correct time taken from the network.

At the end of the connection attempt the method returns the result code:

* 0 No errors.
* 1 Failed WiFi connection.
* 2 Failed cellular connection.
* 3 Failed getting the device name from Particle cloud.
* 4 Failed MQTT connection.

`bool MQTTconnect(String id, String login, String secret)` Manually connects to MQTT and returns the success/failure of the connection, it is only callable if the MQTT setting is enabled.

`bool isConnected()` Returns the status of the connection, `false` if any of WiFi, cellular, MQTT or Particle are disconnected.

`void disconnect()` Manually disconnects from the network.

`void keepAlive()` Keeps the connection/s alive by calling their respective background processes. Necessary in long loops.

#### Publishing

`void setTopicRoot(String topicRootArg)` Sets the topic root used for publishing data to `topicRootArg`, automatically set during `connect()`

`void publish(String topic, T payload)` Publishes the payload under topic `[topicRoot]/[topic]`. The payload can be of type `int`, `float`, `bool`, `String`, `char*` or `const char*` and is automatically formatted.

`void publishInfo()` Publishes information about the device:

* `serial`: device serial number
* `deviceos`: deviceOS version
* `name`: device name
* `branch`: code branch (if set)
* `version`: code version (if set)
* `settings`: settings string
* `start_time`: start time
* `runtime`: time since boot (milliseconds)

And about the connection:

* `connection_type`: "wifi" or "cellular"
* `mac`: the mac address of the device (if using WiFi)
* `ssid`: SSID of the network (if using WiFi)
* `ip`: the IP of the device on the network.
* `rssi`: RSSI of the network
* `bluetooth_scan_timeout`: the timeout of the Bluetooth scan (if enabled)

#### Subscription

* `bool subscribeParticle(String topic, void (*handler)(T, T))`
Subscribe to a topic via Particle. The topic is added to `topicRoot`. A handler needs to be provided that takes two `String` arguments or two `const char*` arguments. Returns the success/failure of the subscription. Refer to [Particle documentation](https://docs.particle.io/reference/device-os/firmware/argon/#particle-subscribe-) for more details.
* `bool subscribeMQTT(String topic)`
Subscribe to a topic (added to `topicRoot`) via MQTT.
* `void (*MQTTCallback) (char* topic, byte* payload, unsigned int length)`
The function that is called when a new message is received through a MQTT subscription. It defaults to `NULL`. Refer to [MQTT documentation](https://pubsubclient.knolleary.net/api.html#callback) for more details on the callback and it structure. A custom function must be assigned before `connect()` is called as it is used when the MQTT client is created.

#### Loop Control & Runtime

`void loopStart()` Starts a new loop by calling `keepAlive()` and increase the `loop` variable by 1.

`void loopClose()` Closes a loop by updating the `loopEnd`variable to the time of the call.

`int runtime()` Returns the time since boot (milliseconds)

## Settings

To give settings to the Management class, a "variables.h" is needed in the `src` folder. All settings need to be written as macros.

The following is a table of all recognized settings and their default values.

* `CONNECTION_TIMEOUT [int]` Milliseconds to wait before returning a connection error. Defaults to 30000ms.
* `BLUETOOTH_SCAN_TIMEOUT [int]` Enable Bluetooth and set scan timeout (milliseconds).
* `PUB_MQTT` Enable and publish on MQTT.
* `PUB_PARTICLE` Enable and publish on Particle.
* `PUB_NAME` Publish using the device name instead of serial.
* `BRANCH [string]` Code branch
* `VERSION [string]` Code version
* `SETTINGS [string]` An optional string containing a list of settings, it is published by the `publishInfo()` method.

## Requirements

If using MQTT, the MQTT library needs to be added as a dependency of the Particle project.
```conf
dependencies.MQTT=0.4.29
```

## Install

To use the library, you need the its files inside the `src` folder.

If you are using a git repository, clone this repository as a submodule inside the `src` folder.
```bash
cd src ; git submodule add git@bitbucket.org:aucbd/particle-management.git
```

If you are not using a git repository, clone this repository inside the `src` folder.
```bash
cd src ; git clone git@bitbucket.org:aucbd/particle-management.git
```

If you are not using a git repository, prefer not to use a submodule or don't have git, download this repository directly inside the `src` folder.

A "variable.h" file must be created in the `src` folder and populated (see [Settings](#settings)).

The final `src` tree should be in this form (the management folder and .ino file can have any name):

```
src/
├── particle-management/
│   ├── management.cpp
│   └── management.h
├── variables.h
└── master.ino
```

## Usage

With the library in the `src` folder and "variables.h" file in place, the Management class can be used in the main .ino file

1. Include the management header (use the local folder of the management library)
```C++
#include "particle-management/management.h"
```
2. Declare a new Management variable globally.
```C++
Management management;
```
3. Set system mode to `MANUAL` or `SEMI_AUTOMATIC` globally or leave it as is (`AUTOMATIC`).
```C++
SYSTEM_MODE(MANUAL);
```
```C++
SYSTEM_MODE(SEMI_AUTOMATIC);
```
4. Call `connect()` inside `setup()` passing the system mode.
```C++
management.connect(SystemMode.mode());
```
5. Call `loopStart()` and `loopClose()` inside `loop()`.
```C++
management.loopStart();

// do something here

management.loopClose();
```
6. Publish data using `publish()`.
```C++
management.publish("topic", variable);
```
