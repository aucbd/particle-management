#include "management.h"
#include "ArduinoJson.h"

/* Tools */
// Strip characters from a String
String stripString(String& str, String c)
{
  while (str.endsWith(c)) str.remove(str.length() - 1);
  while (str.startsWith(c)) str.remove(0);

  return str;
}

// Handle disconnection system events
void eventHandler(system_event_t event)
{
  if (event == cloud_status_disconnected)
    System.reset();
  else if (event == network_status_disconnected)
    System.reset();
}

// Serialise a JsonDocument
template <typename TSource>
String json2string(TSource& object)
{
  String objectString = "";
  serializeJson(object, objectString);
  return objectString;
}

////////////////////////////
/* Constructor            */

// Initialise LED blinker and system events handler, and start serial
Management::Management()
{
  this->blinkLED = LEDStatus(RGB_COLOR_BLUE, LED_PATTERN_BLINK, 100, LED_PRIORITY_IMPORTANT);

  System.on(all_events, eventHandler);

  Serial.begin();
}

////////////////////////////
/* Serial functions       */

void Management::serialInfo()
{
  Serial.println("MANAGEMENT INFO");
  Serial.println("version: " + this->version);
  Serial.println("branch: " + this->branch);
  Serial.println("platform: " + this->platform);
  Serial.println("project: " + this->project);
  Serial.println("serial: " + this->serial);
  Serial.println("name: " + this->name);
  Serial.println("topicRoot: " + this->topicRoot);
  Serial.println("controlTopic: " + this->controlTopic);
  Serial.println("useParticle:" + this->useParticle);
  Serial.println("useParticleName:" + this->useParticleName);
  Serial.println("useMQTT:" + this->useMQTT);
  if (this->useMQTT) {
    Serial.println("MQTTHost: " + String(this->MQTTHost));
    Serial.println("MQTTPort: " + String(this->MQTTPort));
    Serial.println("MQTTUser: " + String(this->MQTTUser));
    Serial.println("MQTTPassword: " + String(this->MQTTPassword));
  }
}

////////////////////////////
/* Connection             */

// Attempts connection. Returns 1 if WiFi/cellular fails, 2 if Particle connection fails,
// 3 if PUB_NAME enabled and name subscription fails, 4 if MQTT enabled and MQTT connection fails,
// 0 if connection succeeds
int Management::connect(int systemMode)
{
  system_tick_t time;

  Serial.println("Connection start");

  // If system mode is 2 (semi-automatic) or 3 (manual), manually connect to WiFi/cellular
  if (systemMode == 2 || systemMode == 3) {
    Serial.println("Connecting to network");
#ifdef WIFI
    // Connect to WiFi
    WiFi.on();
    WiFi.connect();

    time = millis();
    while (!WiFi.ready() && millis() - time <= CONNECTION_TIMEOUT) {
      Particle.process();
      delay(500);
    }
    if (!WiFi.ready()) return 1;
#elif defined(CELLULAR)
    // Connect to cellular
    Cellular.on();
    Cellular.connect();

    time = millis();
    while (!Cellular.ready() && millis() - time <= CONNECTION_TIMEOUT) {
      Particle.process();
      delay(500);
    }
    if (!Cellular.ready()) return 1;
#endif
  }

  // Connect to Particle cloud if enabled or needed
  if (this->useParticle || this->useParticleName) {
    Particle.connect();

    time = millis();
    while (!Particle.connected() && millis() - time <= CONNECTION_TIMEOUT) {
      Particle.process();
      delay(500);
    }

    if (!Particle.connected()) return 2;
  }

  // Get device name if enabled
  if (this->useParticleName) {
    String nameTmp = "";
    Particle.subscribe("particle/device/name", [&](String t, String d) { nameTmp = d; });
    Particle.publish("particle/device/name");

    time = millis();
    while (nameTmp == "" && millis() - time <= CONNECTION_TIMEOUT) {
      Particle.process();
      delay(500);
    }

    if (nameTmp == "")
      return 3;
    else
      this->name = nameTmp;

    Particle.unsubscribe();
  }

  if (this->useParticle) this->subscribeParticle(this->controlTopic, this->ParticleCallback);

  // Connect to MQTT if enabled
  if (this->useMQTT) {
    this->MQTTClient = new MQTT(this->MQTTHost, this->MQTTPort, this->MQTTCallback, 1 << 10);

    srand(Time.now());
    int r = rand() % 99999 + 1;
    time = millis();
    while (!this->connectMQTT(this->serial + String(r), this->MQTTUser, this->MQTTPassword) && millis() - time <= CONNECTION_TIMEOUT) {
      Particle.process();
      r = rand() % 99999 + 1;
      delay(500);
    }

    if (!this->MQTTClient->isConnected()) return 4;

    this->subscribeMQTT(this->controlTopic);
  }

  // Set BLE scan timeout to BLUETOOTH_SCAN_TIMEOUT if defined
  if (this->bluetoothScanTimeout > 0) BLE.setScanTimeout(this->bluetoothScanTimeout / 10);

  // Set topicRoot
  this->setTopicRoot(this->project + "/" + name + "/");

  // Initialize starting time
  for (int i = 0; Time.year() <= 2001 && i < 5; i++) {
    this->keepAlive();
    delay(500);
  }

  this->startTime = Time.now() - (millis() / 1000);

  return 0;
}

// Connect to MQTT
bool Management::connectMQTT(String id, String login, String secret)
{
  return MQTTClient->connect(id, login, secret);
}

// Check connection status of all services
bool Management::isConnected()
{
  bool connectStatus = true;

#ifdef WIFI
  connectStatus = connectStatus && WiFi.ready();
#elif defined(CELLULAR)
  connectStatus = connectStatus && Cellular.ready();
#endif

  connectStatus = connectStatus && (!this->useMQTT || MQTTClient->isConnected());
  connectStatus = connectStatus && (!this->useParticle || Particle.connected());

  return connectStatus;
}

// Disconnect from all services
void Management::disconnect()
{
  if (this->useMQTT && MQTTClient->isConnected()) MQTTClient->disconnect();
  if (Particle.connected()) Particle.disconnect();

#ifdef WIFI
  WiFi.disconnect();
#elif defined(CELLULAR)
  Cellular.disconnect();
#endif
}

// Keep connections alive in the background
void Management::keepAlive()
{
  if (this->useMQTT) this->MQTTClient->loop();
  Particle.process();
}

////////////////////////////
/* Auxiliary functions    */

// Update loop counter and keep connections alive
void Management::loopStart()
{
  this->loop++;

  this->keepAlive();
}

// Store time when last loop ended
void Management::loopClose()
{
  this->loopEnd = Time.now();
}

// Return milliseconds since startup
int Management::runtime()
{
  return ((Time.now() - (this->startTime)) * 1000) + (millis() % 1000);
}

// Blink the status LED
bool Management::blink()
{
  this->blinkLED.setActive(!(this->blinkLED.isActive()));
  return this->blinkLED.isActive();
}

////////////////////////////
/* Publish & Subscribe    */

// Set topicRoot
void Management::setTopicRoot(String topicRootArg) { this->topicRoot = stripString(topicRootArg, "/") + "/"; }

// Publish a value under topicRoot + topic
void Management::publish(String topic, int payload) { this->publishValue(topic, payload); }
// Publish a value under topicRoot + topic
void Management::publish(String topic, float payload) { this->publishValue(topic, payload); }
// Publish a value under topicRoot + topic
void Management::publish(String topic, String payload) {this->publishRaw(topic, "{\"value\":\"" + payload.replace("\"", "\\\"") + "\"}");}
// Publish a value under topicRoot + topic
void Management::publish(String topic, char* payload) { this->publishValue(topic, String(payload)); }
// Publish a value under topicRoot + topic
void Management::publish(String topic, const char* payload) { this->publishValue(topic, String(payload)); }
// Publish a value under topicRoot + topic
void Management::publish(String topic, bool payload) { this->publishValue(topic, payload); }

// Publish formatting the payload as as {"value": <value>}
template <class TValue>
void Management::publishValue(String topic, TValue value)
{
  StaticJsonDocument<JSON_OBJECT_SIZE(1)> payload;
  payload["value"] = value;
  this->publishRaw(topic, json2string(payload));
}

// Publish a String value without formatting
void Management::publishRaw(String topic, String payload)
{
  stripString(topic, "/");
  if (this->useMQTT) MQTTClient->publish(this->topicRoot + topic, payload);
  if (this->useParticle) Particle.publish(this->topicRoot + topic, payload);
}

// Subscribe to a Particle topic and set a callback
bool Management::subscribeParticle(String topic, void (*handler)(String, String))
{
  return Particle.subscribe(this->topicRoot + stripString(topic, "/"), handler);
}
// Subscribe to a Particle topic and set a callback
bool Management::subscribeParticle(String topic, void (*handler)(const char*, const char*))
{
  return Particle.subscribe(this->topicRoot + stripString(topic, "/"), handler);
}

// Subscribe to a topic via MQTT
bool Management::subscribeMQTT(String topic)
{
  return this->MQTTClient->subscribe(this->topicRoot + stripString(topic, "/"));
}

////////////////////////////
/* Callbacks              */

// Callback to control system via remote commands
void Management::controlCallback(String topic, String commandSerialized)
{
  if (String(topic) != this->topicRoot + this->controlTopic) return;

  DynamicJsonDocument com(JSON_OBJECT_SIZE(4) + 150);
  deserializeJson(com, commandSerialized.c_str());

  String token = com["token"];
  String command = com["command"];
  String key = com["key"];
  String value = com["value"];
  int step = com["step"];

  const int capacityReply = JSON_OBJECT_SIZE(6);
  const int capacity = JSON_OBJECT_SIZE(1) + capacityReply * 2;
  DynamicJsonDocument response(capacity);

  JsonObject reply = response.createNestedObject("value");

  reply["token"] = token;

  this->publishRaw("response", json2string(response));

  reply["time"] = Time.now();
  reply["success"] = 1;
  reply["reply"] = nullptr;
  reply["command"] = command;
  reply["step"] = step;

  bool reboot = false;

  if (command == "reset")
    reboot = true;
  else if (command == "blink")
    reply["reply"] = int(this->blink());
  else if (command == "runtime")
    reply["reply"] = this->runtime();
  else if (command == "info")
    this->publishInfo();
#ifdef WIFI
  else if (command == "wifi") {
    if (!key.length()) {
      reply["success"] = 0;
      reply["reply"] = "empty ssid";
    } else if (value.length()) {
      WiFi.setCredentials(key, value);
      reboot = true;
    } else {
      WiFi.setCredentials(key);
      reboot = true;
    }
  }
#endif
  else {
    reply["success"] = 0;
    reply["reply"] = "unknown";
  }

  this->publishRaw("response", json2string(response));

  if (reboot) {
    delay(500);
    System.reset();
  }
}

// Particle wrapper for control callback
void Management::ParticleControlCallback(String topic, String payload)
{
  this->controlCallback(topic, payload);
}

// MQTT wrapper for control callback
void Management::MQTTControlCallback(char* topic, byte* payload, unsigned int length)
{
  char p[length + 1];
  memcpy(p, payload, length);
  p[length] = NULL;
  String msg_str = String(p);

  this->controlCallback(String(topic), msg_str);
}

// Publish general system information
void Management::publishInfo(String topic)
{
  const int capacityCommands = JSON_ARRAY_SIZE(5);
  const int capacityStreams = JSON_ARRAY_SIZE(40);
  const int capacityInfo = JSON_OBJECT_SIZE(8) + capacityCommands + capacityStreams;
  const int capacity = JSON_OBJECT_SIZE(1) + capacityInfo;
  DynamicJsonDocument payload(capacity);

  JsonObject info = payload.createNestedObject("value");

  info["version"] = this->version;
  info["branch"] = this->branch;
  info["platform"] = this->platform;
  info["serial"] = this->serial;
  info["project"] = this->project;
  info["name"] = this->name;

  JsonArray info_commands = info.createNestedArray("commands");
  JsonArray info_streamTopics = info.createNestedArray("streamTopics");

  for (auto&& command : this->commands) info_commands.add(command);
  for (auto&& stream : this->streams) info_streamTopics.add(stream);

  this->publishRaw(topic, json2string(payload));
}
