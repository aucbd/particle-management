#pragma once

/* Particle libraries */
#include "Particle.h"

#include <Wire.h>

/* External libraries */
#include "MQTT/MQTT.h"
#include <MQTT.h>
#include <vector>

#define STRING2(x) #x
#define STRING(x) STRING2(x)

// Settings
#ifndef CONNECTION_TIMEOUT
#define CONNECTION_TIMEOUT 30000
#endif
#if PLATFORM_ID == PLATFORM_SPARK_CORE
#define PLATFORM "spark_core"
#define WIFI
#elif PLATFORM_ID == PLATFORM_SPARK_CORE_HD
#define PLATFORM "spark_core_hd"
#define WIFI
#elif PLATFORM_ID == PLATFORM_PHOTON_DEV
#define PLATFORM "photon_dev"
#define WIFI
#elif PLATFORM_ID == PLATFORM_PHOTON_PRODUCTION
#define PLATFORM "photon"
#define WIFI
#elif PLATFORM_ID == PLATFORM_ARGON
#define PLATFORM "argon"
#define WIFI
#elif PLATFORM_ID == PLATFORM_ELECTRON_PRODUCTION
#define PLATFORM "electron"
#define CELLULAR
#elif PLATFORM_ID == PLATFORM_BORON
#define PLATFORM "boron"
#define CELLULAR
#else
// Throw a compile-time error if neither WiFi nor cellular is selected and cannot be inferred from platform
#pragma message("PLATFORM_ID " STRING(PLATFORM_ID))
#error "UNSUPPORTED PLATFORM"
#endif

class Management {
  private:
  LEDStatus blinkLED;
  MQTT* MQTTClient;
  bool connectMQTT(String, String, String);
  template <typename TValue>
  void publishValue(String, TValue);

  public:
  Management();

  int connect(int = 1);
  bool isConnected(void);
  void disconnect(void);
  void keepAlive();

  void setTopicRoot(String);
  void publish(String, int);
  void publish(String, float);
  void publish(String, String);
  void publish(String, char*);
  void publish(String, const char*);
  void publish(String, bool);
  void publishRaw(String, String);
  void publishInfo(String = "info");

  bool subscribeParticle(String, void (*)(String, String));
  bool subscribeParticle(String, void (*)(const char*, const char*));
  bool subscribeMQTT(String);

  void ParticleControlCallback(String, String);
  void MQTTControlCallback(char*, byte*, unsigned int);
  void controlCallback(String, String);
  void (*ParticleCallback)(String, String);
  void (*MQTTCallback)(char*, byte*, unsigned int) = NULL;

  void loopStart();
  void loopClose();
  int runtime();

  void serialInfo();

  bool blink();

  String version; // Version
  String branch; // Branch
  String platform = PLATFORM; // Platform
  String project; // Project ID
  String serial = System.deviceID(); // Device serial number (hex)
  String name; // Device name (defaults to serial)
  String topicRoot; // Root topic used for publishing
  String controlTopic = "control"; // Topic used for control callbacks
  bool useParticle = true; // Connect to Particle if true
  bool useParticleName = false; // Get name from Particle if true
  bool useMQTT = false; // Connect to MQTT if true
  char* MQTTHost; // MQTT Host
  int MQTTPort; // MQTT Port
  char* MQTTUser; // MQTT User
  char* MQTTPassword; // MQTT Password
  int bluetoothScanTimeout = 0; // Set bluetooth scan timeout to value if above 0
#ifdef WIFI
  std::vector<String> commands = { "reset:::", "blink:::number", "runtime:::number", "info:::", "wifi:string:string:string" }; // Control commands
#else
  std::vector<String> commands = { "reset:::", "blink:::number", "runtime:::number", "info:::" }; // Control commands
#endif
  std::vector<String> streams; // Streams used for publishing
  int startTime = 0; // Device start time
  int loop = 0; // Number of current loop
  int loopEnd = 0; // End time of last loop (seconds)
};
